# Calendar app
Here you will find informatíon about the calendar app.

As you can see there is create-react-app.md that is part of the installation of Create React App,
which was as a tool to develop this app.

If you want to learn more about Create React App you can go to: 
(https://github.com/facebook/create-react-app)


### Source code

The source code is available in Bitbucket at this url:
https://bitbucket.org/jprb79/calendar/


### How to run the app

To run this app, you must copy the files from the folder `build/`
to a public folder at a web server of your preference: apache, nginx, etc.
