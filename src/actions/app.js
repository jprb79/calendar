import {
  getCities as selectCities,
  getWeather as selectWeather
} from '../webapi/'
export const APP_REMINDER_SHOW = 'APP_REMINDER_SHOW'
export const APP_REMINDER_HIDE = 'APP_REMINDER_HIDE'
export const APP_CITIES_REQUEST = 'APP_CITIES_REQUEST'
export const APP_CITIES_RESPONSE = 'APP_CITIES_RESPONSE'
export const APP_CITIES_ERROR = 'APP_CITIES_ERROR'
export const APP_WEATHER_REQUEST = 'APP_WEATHER_REQUEST'
export const APP_WEATHER_RESPONSE = 'APP_WEATHER_RESPONSE'
export const APP_REMINDER_SAVE = 'APP_REMINDER_SAVE'
export const APP_REMINDER_DELETE = 'APP_REMINDER_DELETE'
export const APP_CITIES_CLEAR = 'APP_CITIES_CLEAR'
export const APP_WEATHER_CLEAR = 'APP_WEATHER_CLEAR'

export const clearCities = _ => {
  return {
    type: APP_CITIES_CLEAR
  }
} 

export const clearWeather = _ => {
  return {
    type: APP_WEATHER_CLEAR
  }
}

export const openReminderModal = (year, month, day, mode, reminderId = null) => {
  return {
    type: APP_REMINDER_SHOW,
    payload: {
      year,
      month,
      day,
      mode,
      reminderId
    }
  }
}

export const hideReminderModal = () => {
  return {
    type: APP_REMINDER_HIDE
  }
}

export const getCities = countryCode => {
  return dispatch => {
    dispatch({
      type: APP_CITIES_REQUEST,
      payload: {
        countryCode
      }
    })
    selectCities(countryCode)
    .then(res => {
      const { body } = res
      if (body) {
        dispatch({
          type: APP_CITIES_RESPONSE,
          payload: {
            countryCode,
            cities: body
          }
        })
      } else {
        dispatch({
          type: APP_CITIES_ERROR,
          payload: {
            err: 'No data found'
          }
        })  
      }
    })
    .catch(err => {
      dispatch({
        type: APP_CITIES_ERROR,
        payload: {
          err          
        }
      })
    })
  }
}

export const getWeather = cityId  => {
  return dispatch => {
    dispatch({
      type: APP_WEATHER_REQUEST
    })
    selectWeather(cityId)
    .then(res => {
      const { body } = res
      if (body && body.weather) {
        dispatch({
          type: APP_WEATHER_RESPONSE,
          payload: {
            cityId,
            weather: body.weather
          }
        })
      }
    })
    .catch(err => {
      console.log('error: ', err)
    })
  }
}

export const updateReminder = (reminderId, y, m, d, hour, minute, subject, color, countryCode, cityId) => {
  const date = `${y}-${m}-${d}`
  return (dispatch) => {
    dispatch({
      type: APP_REMINDER_DELETE,
      payload: {
        reminderId,
        date
      }
    })
    dispatch(saveReminder(y, m, d, hour, minute, subject, color, countryCode, cityId))
  }
}
export const saveReminder = (y, m, d, hour, minute, subject, color, countryCode, cityId) => {
  const date = `${y}-${m}-${d}`
  const reminderId = `${y}${m}${d}${hour}${minute}-${Date.now()}`
  return (dispatch, getState) => {
    const state = getState()
    const { cities, weather: we } = state.app
    const weather = (we && we[0] && we[0].main) || 'unknown'
    const city = cities && cities.filter(c => c.id === cityId)
    const cityName = (city && city[0] && city[0].name) || 'unknown'
    dispatch({
      type: APP_REMINDER_SAVE,
      payload: {
        reminderId,
        date,      
        hour,
        minute,
        subject,
        color,
        cityId,
        cityName,
        weather,
        countryCode
      }
    })
  }
}
