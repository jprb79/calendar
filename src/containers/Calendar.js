import React, { Component } from 'react'
import CalendarView from '../components/Calendar'
import { Provider } from 'react-redux'
import { getStore } from '../configureStore'

class Calendar extends Component {
  render () {
    const store = getStore()
    return (
      <Provider store={store}>
        <CalendarView />
      </Provider>
    )
  }
}

export default Calendar