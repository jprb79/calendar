import React, { Component } from 'react'
import DaysRendererView from '../components/DaysRenderer'
import moment from 'moment'

class DaysRenderer extends Component {
  constructor (props) {
    super(props)
    this.state = {
      monthStart: 0,
      monthStartDay: 0,
      monthEnd: 0,
      lastMonthEnd: 0
    }
  }

  componentDidMount () {
    const monthStartDayOfWeek = moment().startOf('month').format('d')
    const monthEnd = moment().endOf('month').format('D')
    const monthEndDayOfWeek = moment().endOf('month').format('d')
    const lastMonthEnd = moment().subtract(1,'months').endOf('month').format('D')
    const currentMonth = moment().format('M')
    const currentYear = moment().format('YYYY')

    this.setState({
      monthStartDayOfWeek,
      monthEnd,
      monthEndDayOfWeek,
      lastMonthEnd,
      currentMonth,
      currentYear
    })
  }

  render () {
    const {
      monthStartDayOfWeek,
      monthEndDayOfWeek,
      monthEnd,
      lastMonthEnd,
      currentMonth,
      currentYear
    } = this.state
    return (
      <DaysRendererView
        monthStartDayOfWeek={monthStartDayOfWeek}
        monthEndDayOfWeek={monthEndDayOfWeek}
        monthEnd={monthEnd}
        lastMonthEnd={lastMonthEnd}
        currentMonth={currentMonth}
        currentYear={currentYear}
        dayClick={this._dayClick}
      />
    )
  }
}
export default DaysRenderer
