import React, { Component } from 'react'
import { connect } from 'react-redux'
import ReminderModalView from '../components/ReminderModal'
import {
  clearCities,
  clearWeather,
  hideReminderModal,
  getCities,
  getWeather,
  saveReminder,
  updateReminder
} from '../actions/app'
import countries from '../lists/country.js'

class ReminderModal extends Component {
  constructor (props) {
    super(props)
    this.state = {
      color: '#ff0000',
      hour: '',
      minute: '',
      subject: '',
      countryCode: '',
      cityId: ''
    }
  }
  _changeCity = evt => {    
    const { getWeather } = this.props
    if (evt.target && evt.target.value) {
      this.setState({
        cityId: evt.target.value
      })
      getWeather(evt.target.value)
    } 
  }

  _changeCountry = evt => {
    const { getCities } = this.props
    if (evt.target && evt.target.value) {
      this.setState({
        countryCode: evt.target.value
      })
      getCities(evt.target.value)
    }
  }

  _changeColor = evt => {
    if (evt.target && evt.target.value) {
      this.setState({
        color: evt.target.value
      })
    }
  }

  _changeHour = evt => {
    if (evt.target && evt.target.value) {
      this.setState({
        hour: evt.target.value
      })
    }
  }

  _changeMinute = evt => {
    if (evt.target && evt.target.value) {
      this.setState({
        minute: evt.target.value
      })
    }
  }

  _changeSubject = evt => {
    if (evt.target && evt.target.value) {
      this.setState({
        subject: evt.target.value
      })
    }    
  }

  _hide = _ => {
    const { hideModal } = this.props
    hideModal()
  }

  _save = _ => {
    const { hour, minute, subject, color, countryCode, cityId } = this.state
    const { saveReminder, updateReminder, year, month, day, mode, reminderId } = this.props
    if (mode === 'new') {
      saveReminder(year, month, day, hour, minute, subject, color, countryCode, cityId)
    } else if(mode === 'edit') {
      updateReminder(reminderId, year, month, day, hour, minute, subject, color, countryCode, cityId)
    }
    this._hide()
  }

  componentDidUpdate (prevProps, prevState) {
    const {
      isModalVisible,
      mode,
      reminderId,
      getCities,
      reminder
    } = this.props
    if (reminderId !== prevProps.reminderId
        && mode === 'edit') {
      const { hour, minute, subject, color, countryCode, cityId } = reminder
      getCities(countryCode)
      this.setState({
        hour,
        minute,
        subject,
        color,
        countryCode,
        cityId
      })
    }
    
    if (mode === 'new' && isModalVisible !== prevProps.isModalVisible) {
      this._clearState()
    }
  }

  _clearState = _ => {
    const { clearCities, clearWeather } = this.props
    clearCities()
    clearWeather()
    this.setState({
      color: '#ff0000',
      hour: '',
      minute: '',
      subject: '',
      countryCode: '',
      cityId: ''
    })
  }

  render () {
    const {
      color,
      subject,
      hour,
      minute,
      countryCode,
      cityId
    } = this.state
    const {
      isModalVisible: isVisible,
      cities,
      weather,
      day,
      month,
      year
    } = this.props
    const enableSave = (
      subject.length < 30 &&
      countryCode &&
      cityId &&
      hour &&
      minute
    )
    return (
      <ReminderModalView
        isVisible={isVisible}        
        countries={countries}
        cities={cities}        
        cityId={cityId}
        countryCode={countryCode}
        color={color}
        hour={hour}
        minute={minute}
        day={day}
        month={month}
        year={year}
        subject={subject}
        changeCity={this._changeCity}
        changeCountry={this._changeCountry}
        changeColor={this._changeColor}
        changeHour={this._changeHour}
        changeMinute={this._changeMinute}
        changeSubject={this._changeSubject}
        weather={weather}
        hide={this._hide}
        save={this._save}
        enableSave={enableSave}
      />
    )
  }
}

export function mapStateToProps (state) {
  const { app } = state
  const {
    isModalVisible,
    cities,
    defaultDate,
    weather,
    mode,
    reminders,
    reminderId
  } = app
  let reminder = {}
  const { day, month, year } = defaultDate
  const date = `${year}-${month}-${day}`
  if (mode === 'edit' && reminderId && reminders[date]) {
    reminder = reminders[date][reminderId]
  }
  return {
    isModalVisible,
    cities,
    day,
    month,
    year,
    weather,
    mode,
    reminder,
    reminderId
  }
}

export function mapDispatchToProps (dispatch) {
  return {
    hideModal: _ => {
      dispatch(hideReminderModal())
    },
    clearCities: _ => {
      dispatch(clearCities())
    },
    clearWeather: _ => {
      dispatch(clearWeather())
    },
    getCities: countryCode => {
      dispatch(getCities(countryCode))
    },
    getWeather: cityId => {
      dispatch(getWeather(cityId))
    },
    saveReminder: (year, month, day, hour, minute, subject, color, countryCode, cityId) => {
      dispatch(saveReminder(year, month, day, hour, minute, subject, color, countryCode, cityId))
    },
    updateReminder: (reminderId, year, month, day, hour, minute, subject, color, countryCode, cityId) => {
      dispatch(updateReminder(reminderId, year, month, day, hour, minute, subject, color, countryCode, cityId))
    },
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ReminderModal)