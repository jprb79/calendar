import React, { Component } from 'react'
import { connect } from 'react-redux'
import DayReminderView from '../components/DayReminder'
import { openReminderModal } from '../actions/app'

class DayReminder extends Component {
  _dayClick = evt => {
    const { year, month, day, openReminderModal } = this.props
    openReminderModal(year, month, day, 'new')
  }

  _eventClick = (evt, id) => {
    const { year, month, day, openReminderModal } = this.props
    evt.stopPropagation()
    openReminderModal(year, month, day, 'edit', id)
  }

  render () {
  	const { day, reminders} = this.props    
    return (
      <DayReminderView
        day={day}
        dayClick={this._dayClick}
        eventClick={this._eventClick}
        reminders={reminders}
      />
    )
  }
}

export function mapStateToProps (state, ownProps) {
  const { app } = state
  const { showReminderModal, reminders } = app
  let { month, day, year } = ownProps
  day = `${day}`.padStart(2, '0')
  month = `${month}`.padStart(2, '0')
  const date = `${year}-${month}-${day}`
  return {
    showReminderModal,
    day,
    month,
    year,
    date,
    reminders: (reminders && reminders[date]) || {}
  }
}

export function mapDispatchToProps (dispatch) {
  return {
    openReminderModal: (y, m, d, mode, id = null) => {
      dispatch(openReminderModal(y, m, d, mode, id))
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DayReminder)