import {
  APP_REMINDER_SHOW,
  APP_REMINDER_HIDE,
  APP_CITIES_RESPONSE,
  APP_WEATHER_RESPONSE,
  APP_REMINDER_SAVE,
  APP_REMINDER_DELETE,
  APP_CITIES_CLEAR,
  APP_WEATHER_CLEAR
} from '../actions/app'

let INITIAL_STATE = {
  defaultDate: { year: '2019', month: '01', day: '01' },
  reminders: {},
  weather: [],
}

const app = (state = INITIAL_STATE, action) => {
  const { type, payload } = action
  switch (type) {
    case APP_REMINDER_SHOW: {
      const { year, month, day, mode, reminderId } = payload
      return {
        ...state,
        isModalVisible: true,
        mode,
        reminderId,
        defaultDate: {
          year,
          month,
          day
        }
      }
    }

    case APP_REMINDER_HIDE: {
      return {
        ...state,
        isModalVisible: false,
        defaultDate: {}
      }
    }

    case APP_CITIES_RESPONSE: {
      const { cities, countryCode } = payload
      return {
        ...state,
        cities,
        countryCode
      }
    }

    case APP_WEATHER_RESPONSE: {
      const { cityId, weather } = payload
      return {
        ...state,
        cityId,
        weather
      }
    }

    case APP_REMINDER_SAVE: {
      const {
        date,
        reminderId,
        hour,
        minute,
        subject,
        color,
        cityId,
        cityName,
        weather,
        countryCode
      } = payload
      const reminders = state.reminders || {}
      const dateReminders = reminders[date] || {}
      return {
        ...state,        
        reminders: {
          ...reminders,
          [date]: {
            ...dateReminders,
            [reminderId]: {
              id: reminderId,
              hour,
              minute,
              subject,
              color,
              countryCode,
              cityId,
              cityName,
              weather
            }
          }
        }
      }
    }

    case APP_REMINDER_DELETE: {
      const { reminderId, date } = payload
      const reminders = state.reminders || {}
      const dateReminders = reminders[date] || {}
      let newDateReminders = { ...dateReminders }
      delete newDateReminders[reminderId]
      return {
        ...state,
        reminders: {
          ...reminders,
          [date]: {
            ...newDateReminders
          }
        }
      }      
    }

    case APP_CITIES_CLEAR: {
      return {
        ...state,
        cities: []
      }
    }

    case APP_WEATHER_CLEAR: {
      return {
        ...state,
        weather: []
      }
    }
    default:
      return state
  }
}

export default app
