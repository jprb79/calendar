import request from 'superagent/lib/client'
const KEY = '62799677d0e2ca3d738d097efcdbd0f0'

export const getCities = countryCode => {
  const url = `http://45.55.189.207/cities/index.php?country=${countryCode}`
  return request.get(url)
}

export const getWeather = cityId => {
  const url = `http://api.openweathermap.org/data/2.5/weather?id=${cityId}&APPID=${KEY}`
  return request.get(url) 
}
