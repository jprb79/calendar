import React from 'react'

const WeekDays = _ => {
  const days = [
  'Sunday', 
  'Monday', 
  'Tuesday', 
  'Wednesday', 
  'Thursday',
  'Friday',
  'Saturday'
  ]

  return (
    <>
      {
      days.map(day =>
        <span className='day-name' key={day}>{day}</span>
      )}
    </>
  )
}

export default WeekDays
