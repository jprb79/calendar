import React from 'react'
import DayReminder from '../containers/DayReminder'

const DaysRenderer = ({
  monthStartDayOfWeek,
  monthEndDayOfWeek,
  monthEnd,
  lastMonthEnd,
  currentMonth,
  currentYear  
}) => {
  let normalDays = [],
    lastMonthDays = [],
    nextMonthDays = []
  if (monthStartDayOfWeek > 0) {
    for (let d = lastMonthEnd - monthStartDayOfWeek + 1; d <= lastMonthEnd; d++) {
      lastMonthDays.push(
        <div
          className='day day--disabled'
          key={d+1000}
        >
          {d}
        </div>
      )
    }
  }

  for (let day = 1; day <= monthEnd; day++) {
    normalDays.push(
      <DayReminder
        year={currentYear}
        month={currentMonth}
        day={day}
        key={`${currentYear}${currentMonth}${day}`}
      />
    )
  }

  if (monthEndDayOfWeek < 6) {
    for (let d = 1; d <= (6 - monthEndDayOfWeek); d++) {
      nextMonthDays.push(
        <div
          className='day day--disabled'
          key={d+2000}
        >{d}
        </div>
      )
    }
  }

  return (
    <>
      {lastMonthDays}
      {normalDays}
      {nextMonthDays}
    </>
  )
}

export default DaysRenderer
