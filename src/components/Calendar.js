import React from 'react'
import moment from 'moment'
import WeekDays from './WeekDays'
import DaysRenderer from '../containers/DaysRenderer'
import ReminderModal from '../containers/ReminderModal'
import '../styles/App.scss'

const Calendar = () => {
  return (
    <>
      <div className='calendar-container'>
        <div className='calendar-header'>
          <h1>
            {moment().format('MMMM')}
          </h1>
          <p>
           {moment().format('YYYY')}
          </p>
        </div>
        <div className='calendar'>
          <WeekDays />
          <DaysRenderer />
        </div>      
      </div>
      <ReminderModal />
    </>
  )  
}

export default Calendar
