import React from 'react'

const DayReminder = ({
  day,
  dayClick,
  eventClick,
  reminders
}) => {
  let arrReminders = []
  if (reminders && Object.values(reminders).length > 0) {    
    arrReminders = 
      Object.values(reminders)
        .sort((a, b) => (a.id >= b.id) ? 1 : -1)
  }
  return (
    <div className='day' onMouseDown={dayClick}>
      {day}
        {arrReminders.map(rem =>
          <div
            key={rem.id}
            style={{ backgroundColor: rem.color }}
            className='reminder reminder--inner'
            onMouseDown={(event) => eventClick(event, rem.id)}>
            [{rem.hour}:{rem.minute}]{`: `} 
            {rem.subject.length > 25 && rem.subject.slice(0, 25) + `...`}
            {rem.subject.length < 25 && rem.subject}
          </div>
        )}
    </div>
  )
}

export default DayReminder
