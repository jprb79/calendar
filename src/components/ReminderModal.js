import React from 'react'
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  FormGroup,
  Label,
  Input,
  Col
} from 'reactstrap'

const Cities = ({ changeCity, cityId, cities }) => {
  return ( 
    <FormGroup>
      <Label for='city'>City:</Label>
      <Input
        type='select'
        onChange={changeCity}
        value={cityId}
      >
        {cities && cities.map(city =>
          <option
            key={city.id}
            value={city.id}
          >
            {city.name}
          </option>  
        )}
        {!cities && <option key={`efgh`}>Select a country first</option>}
      </Input>
    </FormGroup>
  )
}

const ReminderModal = ({
  className,
  isVisible,
  hide,
  save,
  cities, 
  countries,
  countryCode,
  cityId,
  changeCountry,
  changeHour,
  changeMinute,
  changeCity,
  changeColor,
  changeSubject,
  color,
  subject,
  weather,
  day,
  month,
  year,
  hour,
  minute,
  enableSave
}) => {
  let hourOptions = [], minuteOptions = []
  for (let i = 0; i <= 23; i++) {
    let hour = `${i}`.padStart(2, '0')
    hourOptions.push(<option value={hour} key={hour}>{hour}</option>)
  }
  for (let i = 0; i <= 55; i = i+5) {
    let min = `${i}`.padStart(2, '0')
    minuteOptions.push(<option value={min} key={min}>{min}</option>)
  }

  return (
     <div>
      <Modal isOpen={isVisible} className={className}>
        <ModalHeader>Reminder for date [{day}-{month}-{year}]</ModalHeader>
        <ModalBody>
          <FormGroup row>
            <Label for='country' sm={2}>Time:</Label>
            <Col sm={3}>
              <Input type='select' defaultValue={hour} onChange={changeHour}>
                {hourOptions}
              </Input>
            </Col>
            <Col sm={3}>
              <Input type='select' defaultValue={minute} onChange={changeMinute}>
                {minuteOptions}
              </Input>
            </Col>
          </FormGroup>
          <FormGroup>            
            <Label for='country'>Country:</Label>
            <Input
              type='select'
              onChange={changeCountry}
              value={countryCode}
            >
              {!countryCode && <option key={`abcd`}>-- Select --</option>}
              {countries && countries.map(country =>
                <option
                  key={country.code}
                  value={country.code}
                >
                  {country.name}
                </option>  
              )}
            </Input>
          </FormGroup>          
          <Cities changeCity={changeCity} cityId={cityId} cities={cities} />
          {weather && weather[0] &&
            <FormGroup row>
              <Label for='country' sm={2}>Weather:</Label>
              <Col sm={6}>
                <Input type='text' value={weather[0].main} disabled />                
              </Col>
            </FormGroup>
          }
          <FormGroup>
            <Label>Subject: (max 30 chars)</Label>
            <Input
              type='textarea'
              defaultValue={subject}
              onChange={changeSubject}
            />
          </FormGroup>
          <FormGroup>
            <Label>Color:</Label><br />
            <input
              type='color'  
              defaultValue={color}
              onChange={changeColor}
            />
          </FormGroup>
        </ModalBody>
        <ModalFooter>
          <Button color='primary' onClick={save} disabled={!enableSave}>
            Save
          </Button>
          {' '}
          <Button color='secondary' onClick={hide}>
            Cancel
          </Button>
        </ModalFooter>
      </Modal>
    </div>
  )
}

export default ReminderModal
