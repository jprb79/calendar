import { applyMiddleware, createStore, compose } from 'redux'
import logger from 'redux-logger'
import thunk from 'redux-thunk'
import rootReducer from './reducers'

const middleware = [thunk, logger]

const store = createStore(
  rootReducer,
  undefined,
  compose(applyMiddleware(...middleware))
)

export function getStore () {
  return store
}
